# frozen_string_literal: true

require 'spec_helper'
require 'ganges/product_service/aspect/credentials_management/credentials_provider'

describe Ganges::ProductService::Aspect::CredentialsManagement::CredentialsProvider do
  let(:namespace) { Minitest::Mock.new }
  let(:credentials_id) { 1 }
  let(:secret) do
    Vault::Secret.new(
      lease_id: 'lease1',
      lease_duration: '300',
      data: { username: 'username', password: 'password' }
    )
  end
  let(:provider) { Ganges::ProductService::Aspect::CredentialsManagement::CredentialsProvider }

  describe '#fetch_credentials' do
    it 'returns credentials' do
      Vault.stub(:logical, namespace) do
        namespace.expect(:read, secret, [credentials_id])
        credentials = provider.fetch_credentials(credentials_id)

        assert_equal credentials.lease_id, secret.lease_id
        assert_equal credentials.lease_duration, secret.lease_duration
        assert_equal credentials.username, secret.data[:username]
        assert_equal credentials.password, secret.data[:password]
      end
    end
  end

  describe '#renew_lease' do
    let(:lease_id) { 'lease1' }
    it 'renews the lease' do
      Vault.stub(:sys, namespace) do
        namespace.expect(:renew, nil, [lease_id])
        provider.renew_lease(lease_id)
      end
    end
  end
end
