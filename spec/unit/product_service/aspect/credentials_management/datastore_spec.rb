# frozen_string_literal: true

require 'spec_helper'
require 'ostruct'
require 'ganges/product_service/aspect/credentials_management/datastore'

describe Ganges::ProductService::Aspect::CredentialsManagement::Datastore do
  let(:credentials_provider) { Minitest::Mock.new }
  let(:credentials_id) { 'database/creds/product_service_readonly' }
  let(:credentials) do
    OpenStruct.new(
      username: 1,
      password: 2,
      lease_id: 'lease1',
      lease_duration: 300
    )
  end
  let(:datastore) { OpenStruct.new(configure: nil) }
  let(:config) { Minitest::Mock.new }
  let(:datastore_manager) do
    Ganges::ProductService::Aspect::CredentialsManagement::Datastore.new(
      credentials_provider: credentials_provider,
      datastore: datastore
    )
  end

  describe '#start' do
    it 'configures the datastore with credentials from the credential provider' do
      credentials_provider.expect(:fetch_credentials, credentials, [credentials_id])

      config.expect(:username=, nil, [credentials.username])
      config.expect(:password=, nil, [credentials.password])

      datastore.stub(:configure, nil, config) do
        datastore_manager.start
      end
    end

    # Not sure how to test this in minitest.
    it 'periodically tels the provider to renew the lease'
  end
end
