# frozen_string_literal: true

require 'spec_helper'
require 'ganges/product_service/aspect/logging'
require 'ostruct'

class FakeRequestHandler

end

describe Ganges::ProductService::Aspect::Logging::RequestHandler do
  before do
    @fake_request_handler = Class.new do
      def get_product(_request, _metadata)
        OpenStruct.new(gsin: 'G01', name: 'GSIN 01', category_ids: [])
      end

      prepend Ganges::ProductService::Aspect::Logging::RequestHandler
    end.new
  end

  describe 'logger setup' do
    it 'creates an ougai logger' do
      assert_instance_of(Ougai::ChildLogger, @fake_request_handler.logger)
    end

    it 'sets the fields correclty' do
      assert_equal({ aspect: 'core' }, @fake_request_handler.logger.with_fields)
    end
  end

  it 'logs the request and response' do
    request = OpenStruct.new(gsin: 'G01')

    mock_logger = Minitest::Mock.new
    mock_logger.expect(:info, nil, ['Handled Request',
                                    { api: 'get_product',
                                      request: '#<OpenStruct gsin="G01">',
                                      response: '#<OpenStruct gsin="G01", name="GSIN 01", category_ids=[]>' }])
    @fake_request_handler.stub(:logger, mock_logger) do
      @fake_request_handler.get_product request, nil
    end
  end
end
