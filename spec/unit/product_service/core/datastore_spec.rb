# frozen_string_literal: true

require 'spec_helper'
require 'ganges/product_service/core/datastore'

class MockDatastore < Ganges::ProductService::Core::Datastore

  # Sequel mocking doesn't support streaming so we cannot test that
  def mock(result)
    @db = Sequel.connect('mock://postgres')
    @db.extension :pg_array
    Sequel.extension :pg_array_ops

    @db.fetch = result
  end

end

describe Ganges::ProductService::Core::Datastore do
  let(:datastore) { Class.new(MockDatastore).instance }

  describe 'default configuration' do
    { address: 'localhost',
      port: '5433',
      database: 'product_service',
      username: 'product_service',
      password: 'product_service' }.each_pair do |field, value|
      describe "for #{field}" do
        it 'is correct' do
          assert_equal value, datastore.configuration.send(field)
        end
      end
    end
  end

  describe '#configure' do
    %i[address port database username password].each do |field|
      it "saves #{field}" do
        datastore.configure do |config|
          config.public_send("#{field}=", field.to_s)
        end

        assert_equal field.to_s, datastore.configuration.send(field)
      end
    end
  end

  # We cannot unit test connecting to the Postgresql database so this
  # has to remain untested
  # describe '#connect' do; end;

  describe '#get_product_by_gsin' do
    describe 'when the gsin exists' do
      it 'returns the product' do
        product_hash = { gsin: 'gsin1', name: 'Test gsin 1', category_ids: [] }
        datastore.mock [product_hash]

        datastore_response = datastore.get_product_by_gsin('gsin1')

        assert_equal product_hash, datastore_response
      end
    end

    describe 'when the gsin doesn\'t exist' do
      it 'returns nil' do
        datastore.mock []

        datastore_response = datastore.get_product_by_gsin('gsin1')

        assert_nil datastore_response
      end
    end
  end

  describe '#get_products_by_category' do
    describe 'when the gsins exists' do
      it 'returns the a dataset containing the products' do
        product_hash1 = { gsin: 'gsin1', name: 'Test gsin 1', category_ids: [1] }
        product_hash2 = { gsin: 'gsin2', name: 'Test gsin 2', category_ids: [1] }
        product_array = [product_hash1, product_hash2]
        datastore.mock product_array

        datastore_response = datastore.get_products_by_category(1)

        assert_equal product_array, datastore_response.all
      end
    end

    describe 'when the gsin doesn\'t exist' do
      it 'returns the an empty array' do
        datastore.mock []

        datastore_response = datastore.get_products_by_category(1)

        assert [], datastore_response
      end
    end
  end

  # Sequel doesn't support streaming in the mock adapter so we cannot
  # test this.
  # describe '#stream_products_by_category' do; end;
end
