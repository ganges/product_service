# frozen_string_literal: true

require 'spec_helper'
require 'ganges/product_service/core/request_handler'

describe Ganges::ProductService::Core::RequestHandler do
  let(:request_handler) { Ganges::ProductService::Core::RequestHandler.new(repository: repository) }
  let(:repository) { Minitest::Mock.new }
  let(:product1) { Ganges::ProductService::Core::Product.new(gsin: 'gsin1', name: 'Test gsin 1', category_ids: []) }
  let(:product2) { Ganges::ProductService::Core::Product.new(gsin: 'gsin2', name: 'Test gsin 2', category_ids: []) }

  describe '#get_product' do
    it 'responds with a single product' do
      repository.expect(:get_product_by_gsin, product1, [product1.gsin])

      request = Ganges::Grpc::GetProductRequest.new(gsin: product1.gsin)
      response = request_handler.get_product(request, {})

      assert_equal product1.gsin, response.gsin
      assert_equal product1.name, response.name
    end
  end

  describe '#get_products_by_category' do
    it 'respond with two products' do
      products = [product1, product2]
      category_id = 1
      expected_products = [to_grpc(product1), to_grpc(product2)]

      repository.expect(:stream_products_by_category, products, [category_id])

      request = Ganges::Grpc::StreamProductsByCategoryRequest.new(category_id: category_id)
      response_products = request_handler.stream_products_by_category(request, {}).each.entries

      assert_equal expected_products, response_products
    end
  end

  def to_grpc(product)
    Ganges::Grpc::ProductResponse.new(gsin: product.gsin,
                                      name: product.name,
                                      category_ids: product.category_ids)
  end
end
