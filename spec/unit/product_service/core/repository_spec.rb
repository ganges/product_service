# frozen_string_literal: true

require 'spec_helper'
require 'ganges/product_service/core/repository'

describe Ganges::ProductService::Core::Repository do
  let(:repository) { Ganges::ProductService::Core::Repository.new(datastore: datastore) }
  let(:datastore) { Minitest::Mock.new }
  let(:product_one) { Ganges::ProductService::Core::Product.new(gsin: 'gsin1', name: 'Test gsin 1', category_ids: []) }
  let(:product_two) { Ganges::ProductService::Core::Product.new(gsin: 'gsin2', name: 'Test gsin 2', category_ids: []) }

  describe '#get_product_by_gsin' do
    it 'when a product is found returns a single product' do
      datastore.expect(:get_product_by_gsin, product_one.to_hash, [product_one.gsin])

      product = repository.get_product_by_gsin(product_one.gsin)

      assert_equal product_one, product
    end

    it 'when a product is not found returns nil' do
      datastore.expect(:get_product_by_gsin, nil, [product_one.gsin])

      product = repository.get_product_by_gsin(product_one.gsin)

      assert_nil product
    end
  end

  describe '#stream_products_by_category' do
    let(:category_id) { 1 }

    it 'when products are found returns an enumerator' do
      datastore.expect(:stream_products_by_category,
                       [product_one.to_hash, product_two.to_hash],
                       [category_id])

      product_enumerator = repository.stream_products_by_category(category_id)

      assert_kind_of Ganges::ProductService::Core::Repository::ProductEnumerator, product_enumerator
    end

    it 'when products are found returns two products' do
      datastore.expect(:stream_products_by_category,
                       [product_one.to_hash, product_two.to_hash],
                       [category_id])

      products = repository.stream_products_by_category(category_id).each.entries

      assert_equal [product_one, product_two], products
    end

    it 'when no products are found returns no products' do
      datastore.expect(:stream_products_by_category,
                       [],
                       [category_id])

      products = repository.stream_products_by_category(category_id).each.entries

      assert_equal [], products
    end
  end
end
