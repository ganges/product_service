# frozen_string_literal: true

require 'spec_helper'
require 'ganges/product_service/core'

class MockDatastore < Ganges::ProductService::Core::Datastore

  # Sequel mocking doesn't support streaming so we cannot test that
  def mock(result)
    @db = Sequel.connect('mock://postgres')
    @db.extension :pg_array
    Sequel.extension :pg_array_ops

    @db.fetch = result
  end

  # Override this method to remove the stream since we cannot support
  # streaming in tests due to sequel limitations
  def stream_products_by_category(category_id)
    get_products_by_category(category_id)
  end

end

describe Ganges::ProductService::Core::RequestHandler do
  let(:datastore) { MockDatastore.instance }
  let(:repository) { Ganges::ProductService::Core::Repository.new(datastore: datastore) }
  let(:request_handler) { Ganges::ProductService::Core::RequestHandler.new(repository: repository) }
  let(:product1) do
    Ganges::ProductService::Core::Product.new(gsin: 'gsin1',
                                              name: 'Test gsin 1',
                                              category_ids: [])
  end
  let(:product2) do
    Ganges::ProductService::Core::Product.new(gsin: 'gsin2',
                                              name: 'Test gsin 2',
                                              category_ids: [])
  end

  it 'responds to get_product with a single product' do
    datastore.mock [{ gsin: 'gsin1', name: 'Test gsin 1' }]

    request = Ganges::Grpc::GetProductRequest.new(gsin: product1.gsin)

    response = request_handler.get_product(request, {})

    assert_equal product1.gsin, response.gsin
    assert_equal product1.name, response.name
  end

  # Note that due to the mock this is not acually using the sequel stream method.
  # Our stream method is literally the get_products_by_category method with
  # sequels .stream method appened to the end of it and we trust that sequel has
  # tested itself well. The rest of our code works with streaming or non-streaming
  # responses from sequel
  it 'responds to stream_products_by_category with two products' do
    datastore.mock [{ gsin: 'gsin1', name: 'Test gsin 1' },
                    { gsin: 'gsin2', name: 'Test gsin 2' }]

    category_id = 1
    expected_products = [to_grpc(product1), to_grpc(product2)]

    request = Ganges::Grpc::StreamProductsByCategoryRequest.new(category_id: category_id)

    response_products = request_handler.stream_products_by_category(request, {}).each.entries

    assert_equal expected_products, response_products
  end

  def to_grpc(product)
    Ganges::Grpc::ProductResponse.new(gsin: product.gsin,
                                      name: product.name,
                                      category_ids: product.category_ids)
  end
end
