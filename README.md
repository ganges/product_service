# Ganges Platform Product Service

This gem provides the Ganges Platform Product Service which is a gRPC server written in ruby that stores and serves product information.

This microservice is designed to be 12Factor App compatible

## Usage

Run the gRPC server using `bundle exec product_service`. See the Environment Variables section
below for more information on what environment variables are required.

### Environment Variables

#### Core

The following environment variables are REQUIRED to be set for the server to function properly.

First up we have the network related environment variable configuration

* ```SERVER_ADDRESS``` Hostname or IP Address the server should listen on.
* ```SERVER_PORT``` Port number the server should listen on.

Next is the datastore (postgresl) configuration

* ```DATASTORE_ADDRESS``` Hostname or IP Address postgresql is listening on
* ```DATASTORE_PORT``` Port number postgresql is listening on
* ```DATASTORE_NAME``` Name of the postgresql database 
* ```DATASTORE_USERNAME``` Username to access the postgresql database 
* ```DATASTORE_PASSWORD``` Password to access the postgresql database 

#### Optional

The following environment variables are optional

* ```SYNC_LOG``` Set stdout to flush logs after every line. Needed for things like foreman. Set this to anything to enable flushing.

#### NewRelic Monitoring

To enable New Relic the following environment variables need to be appropriately set. See https://docs.newrelic.com/docs/agents/ruby-agent/configuration/ruby-agent-configuration#autostart for more information.

* ```NEW_RELIC_AGENT_ENABLED``` Set this to anything to enable the NewRelic agent.
* ```NEW_RELIC_LICENSE_KEY``` NewRelic license key.
* ```NEW_RELIC_LOG_LEVEL``` The log level for the NewRelic agent.
* ```NEW_RELIC_APP_NAME``` The application name for reporting metrics under.

### Hashicorp Vault

* ```VAULT_ADDR``` Set this to the vault server's address to enable Vault

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

TODO: Update the following release instructions depending on how we release the service (e.g. maybe Gemfury):

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

### Using Foreman

Use the following .env file with foreman to run an instance locally without any Hashicorp dependencies (This assumes you are running on ubuntu with the default ubuntu ports for postgresql)

```
SERVER_ADDRESS=0.0.0.0
SERVER_PORT=5000
SYNC_LOG=true
DATASTORE_ADDRESS=localhost
DATASTORE_PORT=5433
DATASTORE_NAME=product_service
DATASTORE_USERNAME=product_service
DATASTORE_PASSWORD=product_service
```

### Setting up Hashicorp Vault

The following commands are used to setup a vault instance talking to your local database. This allows you to test Vault integration locally.

```
vault server -dev

export VAULT_ADDR='http://127.0.0.1:8200'
```
Then in a separate command-line run the following command

```
vault secrets enable database

vault write database/config/product_service \
plugin_name=postgresql-database-plugin \
allowed_roles="product_service_readonly" \
connection_url="postgresql://product_service:product_service@localhost:5433/product_service"

vault write database/roles/product_service_readonly \
db_name=product_service \
creation_statements="CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'; GRANT SELECT ON ALL TABLES IN SCHEMA public TO \"{{name}}\";" \
default_ttl="30m" \
max_ttl="1h"
```

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/ganges/product_service .

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
