# frozen_string_literal: true

Sequel.migration do
  up do
    create_table(:products) do
      String :gsin, primary_key: true
      String :name, null: false
      column 'category_ids', 'integer[]'
    end
  end

  down do
    drop_table(:products)
  end
end
