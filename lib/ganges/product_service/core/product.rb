# frozen_string_literal: true

module Ganges
  module ProductService
    module Core
      # Product returned by the ProductRepository.
      class Product

        attr_reader :gsin, :name, :category_ids

        def initialize(gsin: nil, name: nil, category_ids: nil)
          @gsin = gsin
          @name = name
          @category_ids = category_ids
        end

        def ==(other)
          other.class == Product &&
            other.gsin == gsin &&
            other.name == name &&
            other.category_ids == category_ids
        end

        def to_hash
          {
            gsin: gsin,
            name: name,
            category_ids: category_ids
          }
        end

      end
    end
  end
end
