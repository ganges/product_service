# frozen_string_literal: true

require 'ganges/product_service/core/product'
require 'ganges/product_service/core/datastore'

module Ganges
  module ProductService
    module Core
      # ProductRepository.
      #
      # This class acts as the interface to the product datastore and hides the actual
      # implementation
      class Repository

        def initialize(datastore: Ganges::ProductService::Core::Datastore.instance)
          @datastore = datastore
        end

        def get_product_by_gsin(gsin)
          row = @datastore.get_product_by_gsin(gsin)
          row ? to_product(row) : nil
        end

        def stream_products_by_category(category_id)
          ProductEnumerator.new(@datastore.stream_products_by_category(category_id))
        end

        def to_product(product_hash)
          category_ids = product_hash[:categories] ? product_hash[:categories] : []
          Ganges::ProductService::Core::Product.new(gsin: product_hash[:gsin],
                                                    name: product_hash[:name],
                                                    category_ids: category_ids)
        end

        # Wraps responses from the datastore
        #
        # Wraps responses from the datastore in an enumerator.
        class ProductEnumerator

          def initialize(product_hashes)
            @product_hashes = product_hashes
          end

          def each
            return enum_for(:each) unless block_given?
            @product_hashes.each do |product_hash|
              category_ids = product_hash[:categories] ? product_hash[:categories] : []
              yield Ganges::ProductService::Core::Product.new(gsin: product_hash[:gsin],
                                                              name: product_hash[:name],
                                                              category_ids: category_ids)
            end
          end

        end

      end
    end
  end
end
