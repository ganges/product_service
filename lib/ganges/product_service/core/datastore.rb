# frozen_string_literal: true

require 'sequel'
require 'singleton'

module Ganges
  module ProductService
    module Core
      # Acts as the interface to the back-end datastore and hides all datastore
      # implementation details from callers. Note that ruby does not support
      # exception chaining we are not wrapping exceptions yet. This will happen
      # when  https://bugs.ruby-lang.org/issues/8257 is fixed.
      class Datastore

        include Singleton

        @configuration = nil
        @db = nil

        def configuration
          @configuration ||= Configuration.new
        end

        def configure
          configuration
          yield(@configuration) if block_given?
        end

        # Contains all the connection configuration required for connectin
        # to a postgresql database. Defaults to localhost with ubuntu presets
        class Configuration

          attr_accessor :address, :port, :database, :username, :password

          def initialize
            @address = 'localhost'
            @port = '5433'
            @database = 'product_service'
            @username = 'product_service'
            @password = 'product_service'
          end

        end

        def connect
          configure
          @db = Sequel.connect(connection_string)
          @db.extension :pg_array, :pg_streaming
          Sequel.extension :pg_array_ops
        end

        def get_product_by_gsin(gsin)
          @db[:products].where(gsin: gsin)
                        .first
        end

        def get_products_by_category(category_id)
          category_contains_id = Sequel.pg_array_op(:category_ids).contains(Array(category_id))
          @db[:products].where(category_contains_id)
                        .order(:gsin)
        end

        def stream_products_by_category(category_id)
          get_products_by_category(category_id).stream
        end

        private

        def connection_string
          "postgres://#{@configuration.username}:#{@configuration.password}@" \
					"#{@configuration.address}:#{@configuration.port}/#{@configuration.database}"
        end

      end
    end
  end
end
