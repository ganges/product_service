# frozen_string_literal: true

require 'product_services_pb'
require 'ganges/product_service/core/repository'

module Ganges
  module ProductService
    module Core
      # The actual class that implements the gRPC interface
      class RequestHandler < Ganges::Grpc::ProductService::Service

        def initialize(repository: Ganges::ProductService::Core::Repository.new)
          @repository = repository
        end

        def get_product(request, _metadata)
          product = @repository.get_product_by_gsin request.gsin
          Ganges::Grpc::ProductResponse.new(gsin: product.gsin,
                                            name: product.name,
                                            category_ids: product.category_ids)
        end

        def stream_products_by_category(request, _metadata)
          results = @repository.stream_products_by_category request.category_id
          ProductEnumerator.new(results).each
        end

        # Wraps responses from the repository
        #
        # Wraps reponses from the repository in an enumerator the converts them to gRPC
        # responses to allow for streaming back to clients.
        class ProductEnumerator

          def initialize(products)
            @products = products
          end

          def each
            return enum_for(:each) unless block_given?
            @products.each do |product|
              yield Ganges::Grpc::ProductResponse.new(gsin: product.gsin,
                                                      name: product.name,
                                                      category_ids: product.category_ids)
            end
          end

        end

      end
    end
  end
end
