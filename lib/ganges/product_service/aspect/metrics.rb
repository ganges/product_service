# frozen_string_literal: true

# If the NewRelic agent is enabled then we assume that we want to send metrics.
# We will also make sure that the other required environment variables are present.
if ENV['NEW_RELIC_AGENT_ENABLED']
  ENV.fetch 'NEW_RELIC_LICENSE_KEY'
  ENV.fetch 'NEW_RELIC_LOG_LEVEL'
  ENV.fetch 'NEW_RELIC_APP_NAME'

  require 'new_relic/agent'

  # This is a temporary Shim to stop loggingg to file while we figure out logging
  require 'logger'
  NewRelic::Agent.manual_start log: Ganges::ProductService.logger.child(aspect: 'metrics')

  require 'ganges/product_service/aspect/metrics/request_handler'
  require 'ganges/product_service/aspect/metrics/repository'
end
