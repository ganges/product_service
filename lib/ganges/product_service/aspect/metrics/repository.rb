# frozen_string_literal: true

module Ganges
  module ProductService
    module Core
      # Apply newrelic metricing to the Repository
      class Repository

        include ::NewRelic::Agent::MethodTracer
        add_method_tracer :get_product_by_gsin

      end
    end
  end
end
