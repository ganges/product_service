# frozen_string_literal: true

module Ganges
  module ProductService
    module Core
      # Apply newrelic metricing to the RequestHandler
      class RequestHandler < Ganges::Grpc::ProductService::Service

        include NewRelic::Agent::Instrumentation::ControllerInstrumentation
        add_transaction_tracer :get_product

      end
    end
  end
end
