# frozen_string_literal: true

require_relative 'credentials_provider'
require_relative '../../core/datastore'

module Ganges
  module ProductService
    module Aspect
      module CredentialsManagement
        # Manages credentials for the Product Service datastore
        class Datastore

          CREDENTIALS_IDENTIFIER = 'database/creds/product_service_readonly'

          def initialize(credentials_provider: CredentialsProvider,
                         datastore: Ganges::ProductService::Core::Datastore.instance)
            @credentials_provider = credentials_provider
            @datastore = datastore
          end

          def start
            credentials = @credentials_provider.fetch_credentials(CREDENTIALS_IDENTIFIER)

            configure_datastore(credentials.username, credentials.password)

            # To be safe, null out the username and password
            credentials.username = nil
            credentials.password = nil

            start_renew_loop(credentials.lease_id, credentials.lease_duration)
          end

          private

          def configure_datastore(username, password)
            @datastore.configure do |config|
              config.username = username
              config.password = password
            end
          end

          def start_renew_loop(lease_id, lease_duration)
            Thread.new do
              sleep lease_duration * 0.9 # sleep for 90% of the lease duration
              @credentials_provider.renew_lease(lease_id)
            end
          end

        end
      end
    end
  end
end
