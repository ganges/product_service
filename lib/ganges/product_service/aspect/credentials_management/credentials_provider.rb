# frozen_string_literal: true

require 'vault'
require 'ostruct'

module Ganges
  module ProductService
    module Aspect
      module CredentialsManagement
        # Interface to the credentials provider
        module CredentialsProvider

          class << self

            def fetch_credentials(credentials_id)
              secret = Vault.logical.read(credentials_id)

              OpenStruct.new(
                username: secret.data[:username],
                password: secret.data[:password],
                lease_id: secret.lease_id,
                lease_duration: secret.lease_duration
              )
            end

            def renew_lease(lease_id)
              Vault.sys.renew lease_id
            end

          end

        end
      end
    end
  end
end
