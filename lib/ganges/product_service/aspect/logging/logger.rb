# frozen_string_literal: true

# Monkey Patching the Ougai classes
module Ougai
  # Monkey Patching the Outgai ChildLogger for NewRelic compatibility
  class ChildLogger

    # A no-op method for NewRelic Agent compatibility
    def level=(level); end

    # A no-op method for NewRelic Agent compatibility
    def formatter=(formatter); end

  end
end

module Ganges
  # Logger methods to be added to classes that need them
  module Logger

    # Logger This is to be set by the logging aspect.
    @logger = nil

    def logger
      @logger
    end

    def logger=(logger)
      @logger = logger
    end

  end
end

module Ganges
  # Parent module for all code that is part of the Product Service.
  module ProductService

    extend Ganges::Logger

  end
end

# Module used by GRPC
module GRPC

  extend Ganges::Logger

end
