# frozen_string_literal: true

module Ganges
  module ProductService
    module Aspect
      module Logging
        # Apply logging to the RequestHandler
        module RequestHandler

          def logger
            @logger ||= Ganges::ProductService.logger.child(aspect: 'core')
          end

          def get_product(request, metadata)
            response = super(request, metadata)
            logger.info('Handled Request', api: 'get_product', request: request.inspect, response: response.inspect)
            response
          end

        end
      end
    end
  end
end
