# frozen_string_literal: true

require 'ougai'
require 'ganges/product_service/aspect/logging/logger'

logger = Ougai::Logger.new(STDOUT)
logger.with_fields = { name: 'ProductService' }
logger.level = Logger::INFO

Ganges::ProductService.logger = logger

GRPC.logger = Ganges::ProductService.logger.child(aspect: 'gRPC')

require 'ganges/product_service/aspect/logging/request_handler'

module Ganges
  module ProductService
    module Aspect
      # Aspect that controls logging to stdout so that we are 12factor app compatible
      module Logging

        def self.apply
          Core::RequestHandler.prepend(Logging::RequestHandler)
        end

      end
    end
  end
end
