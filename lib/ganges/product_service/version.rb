# frozen_string_literal: true

module Ganges
  module ProductService

    # Created automatically when building a gem.
    # This may well be un-needed but depends how we will deploy the Product Service.
    VERSION = '0.1.0'

  end
end
