# frozen_string_literal: true

require 'ganges/product_service/version'
require 'ganges/product_service/core/request_handler'

# Parent modeule for the entire Ganges platform. ALL ganges code will be namespaced under this
# module. This includes gRPC auto-generated code.
module Ganges
  # Parent module for all code that is part of the Product Service.
  module ProductService
    # All core logic goes into this namespace. This is the logic that ONLY implements the
    # business required functions of the service. Operational code (Logging, Metrics etc.) do
    # NOT belong to this category
    module Core
    end
  end
end
