# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ganges/product_service/version'

Gem::Specification.new do |spec|
  spec.name          = 'ganges-product_service'
  spec.version       = Ganges::ProductService::VERSION
  spec.authors       = ['Jeffrey Jones']
  spec.email         = ['jeff@jones.be']

  spec.summary       = 'Ganges Platform gRPC service'
  spec.homepage      = 'https://gitlab.com/ganges/product_service'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.' unless spec.respond_to?(:metadata)
  spec.metadata['allowed_push_host'] = 'http://mygemserver.com'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'ganges-grpc-ruby-code', '0.1.0'
  spec.add_dependency 'grpc', '~>1.18.0'
  spec.add_dependency 'newrelic_rpm', '~>6.0.0'
  spec.add_dependency 'ougai', '~>1.7.0'
  spec.add_dependency 'sequel', '~>5.17.0'
  spec.add_dependency 'sequel_pg', '~>1.11.0'
  spec.add_dependency 'vault', '~> 0.1'

  spec.add_development_dependency 'benchmark-ips', '~> 2.7'
  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'rake', '~> 12.3.0'
  spec.add_development_dependency 'simplecov', '~> 0.15'
end
