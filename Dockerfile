FROM ruby:2.5

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

ARG gemfury_key

RUN gem install bundler && bundle config gem.fury.io ${gemfury_key} && bundle install --deployment && bundle exec rake spec

# Make port 80 available to the world outside this container
EXPOSE 5000

# Run app.py when the container launches
CMD ["ruby", "exe/product_service"]
